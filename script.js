const userList = document.getElementById('userList');
const userLimitSelect = document.getElementById('userLimit');

function loadUsers(limit) {
    fetch(`https://jsonplaceholder.typicode.com/users?_page=1&_limit=${limit}`)
        .then(response => response.json())
        .then(users => {
            userList.innerHTML = '';
            users.forEach(user => {
                const listItem = document.createElement('li');
                let userInfo = `Ім'я: ${user.name}, Прізвище: ${user.name}, Пошта: ${user.email}, Сайт: <a href="${user.website}">${user.website}</a>`;
                listItem.innerHTML = userInfo;
                if (user.id % 2 === 0) {
                    listItem.classList.add('grayBackground');
                }
                userList.appendChild(listItem);
            });
        })
        .catch(error => console.error('Помилка завантаження даних:', error));
}

loadUsers(2);

userLimitSelect.addEventListener('change', function () {
    const selectedLimit = this.value;
    loadUsers(selectedLimit);
});

